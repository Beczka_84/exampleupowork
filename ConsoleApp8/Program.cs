﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(() => test()).GetAwaiter().GetResult();
        }



        static async void test()
        {
            var test = await Consumer.Go<bool>();
        }
    }

    public static class Consumer
    {
        private delegate bool Redo();
        private static bool callbackFunction;

        public static async Task<T> Go<T>()
        {
            var someLibrary = new SomeLibrary();
            callbackFunction = await Async<bool>();
            return await someLibrary.SomeMethod(Async<T>);
        }

        public static async Task<T> Async<T>()
        {
            return await Task.FromResult(default(T));
        }
    }

    public class SomeLibrary
    {
        public async Task<T> SomeMethod<T>(Func<Task<T>> func)
        {
            return await func.Invoke();
        }
    }
}
