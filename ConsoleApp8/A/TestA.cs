﻿using System;
using System.IO;
using System.Runtime.Caching;
using Newtonsoft.Json;

namespace ConsoleApp8.A
{
    public class MemoryCacheItemDto
    {
        public Type Type { get; set; }
        public Address Address { get; set; }
    }

    public abstract class BaseClass
    {
        protected BaseClass()
        {
            var path = Directory.CreateDirectory(FolderPath);
        }

        protected string FolderPath = @"D:\test";

        public string Id { get; set; }

        [JsonIgnore]
        public Address Address { get; set; }

        public void Save()
        {
            Id = Guid.NewGuid().ToString();
            MemoryCache.Default.Add(Id, new MemoryCacheItemDto() { Address = this.Address, Type = this.GetType()}, DateTimeOffset.MaxValue);
            var path = System.IO.Directory.CreateDirectory(FolderPath);
            File.WriteAllText($"{FolderPath}\\{Id}.json", JsonConvert.SerializeObject(this));
        }

        public void Delete()
        {
            MemoryCache.Default.Remove(Id);
            File.Delete($"{FolderPath}\\{Id}.json");
            Id = null;
        }

        public static dynamic Find(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                return null;
            }

            var memory = MemoryCache.Default[id]  as MemoryCacheItemDto;

            using (StreamReader file = File.OpenText($@"D:\test\{id}.json"))
            {
                JsonSerializer serializer = new JsonSerializer();
                dynamic obj = serializer.Deserialize(file, memory.Type);
                obj.Address = memory.Address;
                return obj;
            }
        }
    }

    public class Address
    {
        public string A { get; set; }
        public string B { get; set; }
        public string C { get; set; }
        public string D { get; set; }

        public Address(string a, string b, string c, string d)
        {
            A = a;
            B = b;
            C = c;
            D = d;
        }
    }

    public class Customer : BaseClass
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
      

        public Customer(string firstName, string lastName, Address address)
        {
            Address = address;
            FirstName = firstName;
            LastName = lastName;
        }

        // i know know :) puropose of this task was to make classes that will pass the tunit tests
        public override bool Equals(object obj)
        {
            if(obj is Customer that)
            {
                return this.Id == that.Id;
            }

            return false;
        }
    }

    public class Company : BaseClass
    {
        public string Name { get; set; }

        public Company(string name, Address address)
        {
            Name = name;
            Address = address;
        }

        // i know know :) puropose of this task was to make classes that will pass the tunit tests
        public override bool Equals(object obj)
        {
            if (obj is Company that)
            {
                return this.Id == that.Id;
            }

            return false;
        }
    }
}