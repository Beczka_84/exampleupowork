﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8.B
{
    class A
    {
        public A()
        {

        }

        private void SolveBinomial()
        {
            string[] args = new string[0];
            SmtpClient client = new SmtpClient(args[0]);
            MailAddress from = new MailAddress("jane@contoso.com", "Jane Clayton", System.Text.Encoding.UTF8);
            MailAddress to = new MailAddress("ben@contoso.com");
            MailMessage message = new MailMessage(from, to);
            message.Body = "Hi Ben, this is Jane, how's it going?";
            //client.SendCompleted += new SendCompletedEventHandler(binomialEventHandler);
            string userState = "test message1";
            client.SendAsync(message, userState);
            message.Dispose();
        }
    }

    class B
    {
        public B()
        {

        }

        private void SolveTrinomial()
        {
            string[] args = new string[0];
            SmtpClient client = new SmtpClient(args[0]);
            MailAddress from = new MailAddress("bob@contoso.com", "Bob Doe", System.Text.Encoding.UTF8);
            MailAddress to = new MailAddress("sarah@contoso.com");
            MailMessage message = new MailMessage(from, to);
            message.Body = "Hi Sarah, here are the calculations...";
            //client.SendCompleted += new SendCompletedEventHandler(trinomialEventHandler);
            string userState = "test message1";
            client.SendAsync(message, userState);
            message.Dispose();
        }
    }
}
